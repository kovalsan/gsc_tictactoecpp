// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BoardRules.h"

#include "TicTacToeCPPBlock.h"
#include "TicTacToeCPPBlockGrid.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlayerBehaviorType : uint8 {
	PBT_USER	UMETA(DisplayName = "Real Player"),
	PBT_AI		UMETA(DisplayName = "AI"),
};

/** Class used to spawn blocks and manage score */
UCLASS(minimalapi)
class ATicTacToeCPPBlockGrid : public AActor {
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

public:
	ATicTacToeCPPBlockGrid();

	/** Number of blocks along each side of grid */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	int32 Size;

	/** Spacing of blocks */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	float BlockSpacing;

	/** Player's Id current turn */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	EPlayerType CurrentPlayerId;

	/** Array of pointers to the blocks */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	TArray<ATicTacToeCPPBlock*> Cells;

	/** Array of cell states */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	TArray<int32> CellStates;

	/** Array of pointers to the blocks */
	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	EBoardState BoardState;

	/** Array of pointers to the blocks */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadWrite)
	EPlayerType FirstTurnPlayerId;

	/** Map for player types */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadWrite)
	TMap<EPlayerType, EPlayerBehaviorType> Players;

	UFUNCTION(Category= Grid, BlueprintImplementableEvent, BlueprintCallable)
	void OnTurnUpdate();

	UFUNCTION(Category = Grid, BlueprintCallable)
	void ResetBoard();

	UFUNCTION(Category = Grid, BlueprintCallable)
	void StartBoard();

protected:
	// Begin AActor interface
	virtual void BeginPlay() override;
	// End AActor interface

public:

	/** Handle the block being clicked */
	void UpdateGrid(int cellId);

	/** Returns Board's interactivity state */
	bool IsInteractable();

	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }

private:
	int	_turnCount;
};



