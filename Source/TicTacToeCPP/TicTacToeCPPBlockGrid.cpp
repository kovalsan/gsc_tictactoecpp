// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeCPPBlockGrid.h"
#include "Components/TextRenderComponent.h"
#include "Engine/World.h"

#define LOCTEXT_NAMESPACE "PuzzleBlockGrid"
#define VICTORY_SEQUENCE_LENGTH	3

ATicTacToeCPPBlockGrid::ATicTacToeCPPBlockGrid() :
	_turnCount(0),
	BoardState(EBoardState::BS_INITIAL),
	FirstTurnPlayerId(EPlayerType::PT_UNDEFINED),
	CurrentPlayerId(EPlayerType::PT_UNDEFINED)
{
	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Set defaults
	Size = 3;
	BlockSpacing = 300.f;

	Players.Reserve(2);
}


void ATicTacToeCPPBlockGrid::BeginPlay() {
	Super::BeginPlay();

	// Number of blocks
	const int32 NumBlocks = Size * Size;
	CellStates.Init(0, NumBlocks);

	// Loop to spawn each block
	for(int32 BlockIndex=0; BlockIndex<NumBlocks; BlockIndex++) {
		const float XOffset = (BlockIndex/Size) * BlockSpacing; // Divide by dimension
		const float YOffset = (BlockIndex%Size) * BlockSpacing; // Modulo gives remainder

		// Make position vector, offset from Grid location
		const FVector BlockLocation = FVector(XOffset, YOffset, 0.f) + GetActorLocation();

		// Spawn a block
		ATicTacToeCPPBlock* NewBlock = GetWorld()->SpawnActor<ATicTacToeCPPBlock>(BlockLocation, FRotator(0,0,0));

		// Tell the block about its owner
		if (NewBlock != nullptr) {
			NewBlock->OwningGrid = this;
			NewBlock->CellId = BlockIndex;
			Cells.Add(NewBlock);
		}
	}
}

void ATicTacToeCPPBlockGrid::UpdateGrid(int cellId) {
	CellStates[cellId] = static_cast<int32>(CurrentPlayerId);
	BoardState = UBoardRules::GetGridState(CellStates, static_cast<int>(CurrentPlayerId));

	if (BoardState == EBoardState::BS_NEXT_TURN)
		CurrentPlayerId = static_cast<EPlayerType>(++_turnCount % 2 + 1);

	OnTurnUpdate();
}

void ATicTacToeCPPBlockGrid::ResetBoard() {
	_turnCount = static_cast<int8>(FirstTurnPlayerId)-1;
	CurrentPlayerId = FirstTurnPlayerId;
	CellStates.Init(0, Size*Size);

	for (auto& cell : Cells) {
		cell->Reset();
	}

	BoardState = EBoardState::BS_NEXT_TURN;
	OnTurnUpdate();
}

void ATicTacToeCPPBlockGrid::StartBoard() {
	if (FirstTurnPlayerId == EPlayerType::PT_UNDEFINED) {
		UE_LOG(LogTemp, Warning, TEXT("Game Board Error: Player's side for the first turn has not been set!"));
		return;
	}

	if (!Players.Contains(EPlayerType::PT_CROSSES) || !Players.Contains(EPlayerType::PT_NOUGHTS)) {
		UE_LOG(LogTemp, Warning, TEXT("Game Board Error: Player types are not initialized!"));
		return;
	}

	ResetBoard();
}

bool ATicTacToeCPPBlockGrid::IsInteractable() {
	bool isNextTurnState = BoardState == EBoardState::BS_NEXT_TURN;
	bool isCurrentPlayerAI = Players[CurrentPlayerId] == EPlayerBehaviorType::PBT_AI;
	bool result = isNextTurnState && !isCurrentPlayerAI;

	return result;
}


#undef LOCTEXT_NAMESPACE
