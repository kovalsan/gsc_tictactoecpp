// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BoardRules.generated.h"

#define VICTORY_SEQUENCE_LENGTH	3

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlayerType : uint8 {
	PT_UNDEFINED = 0	UMETA(DisplayName = "Undefined"),
	PT_CROSSES = 1		UMETA(DisplayName = "Crosses"),
	PT_NOUGHTS = 2		UMETA(DisplayName = "Noughts"),
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EBoardState : uint8 {
	BS_VICTORY = 0	UMETA(DisplayName = "Victory of the current player"),
	BS_NEXT_TURN	UMETA(DisplayName = "Next turn for the current player"),
	BS_DRAW			UMETA(DisplayName = "Draw"),
	BS_INITIAL		UMETA(DisplayName = "Initial state of the Board")
};

UCLASS()
class TICTACTOECPP_API UBoardRules : public UBlueprintFunctionLibrary {
	GENERATED_BODY()
public:	
	static EBoardState GetGridState(const TArray<int32>& cellStates, const int playerId) {
		int32 size = sqrt(cellStates.Num());
		for (int x = 0; x < size; x++) {
			int vSeqLength = 0;
			int hSeqLength = 0;

			for (int y = 0; y < size; y++) {
				int id_v = y * size + x;
				int id_h = x * size + y;

				vSeqLength = cellStates[id_v] == playerId ? vSeqLength + 1 : 0;
				hSeqLength = cellStates[id_h] == playerId ? hSeqLength + 1 : 0;

				if (vSeqLength >= VICTORY_SEQUENCE_LENGTH || hSeqLength >= VICTORY_SEQUENCE_LENGTH) {
					return EBoardState::BS_VICTORY;
				}
			}
		}

		int mainSeqLength = 0;
		int sideSeqLength = 0;

		for (int i = 0; i < size; i++) {
			int id_main = i * size + i;
			int id_side = (size - i - 1) * size + i;

			mainSeqLength = cellStates[id_main] == playerId ? mainSeqLength + 1 : 0;
			sideSeqLength = cellStates[id_side] == playerId ? sideSeqLength + 1 : 0;

			if (mainSeqLength >= VICTORY_SEQUENCE_LENGTH || sideSeqLength >= VICTORY_SEQUENCE_LENGTH) {
				return EBoardState::BS_VICTORY;
			}
		}

		bool isDraw = true;
		for (auto cell = cellStates.CreateConstIterator(); cell; ++cell) {
			if (*cell == 0) {
				isDraw = false;
				break;
			}
		}

		if (isDraw) {
			return EBoardState::BS_DRAW;
		}
		else {
			return EBoardState::BS_NEXT_TURN;
		}
	};
};
