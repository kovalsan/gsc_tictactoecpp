// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "BoardRules.h"

#include "BoardSolver.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTurnAICompleteEventDelegate, int, cellId);

UCLASS( ClassGroup=(Custom), meta=(Blueprintable) )
class TICTACTOECPP_API UBoardSolver : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBoardSolver();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	void PerformNextTurn(const TArray<int32 >& board, int playerId);

	UPROPERTY(BlueprintAssignable, Category = "AI")
	FOnTurnAICompleteEventDelegate OnTurnAIComplete;

private:
	int32 maxSearch(TArray<int32 >& board, int playerId);
	int32 minSearch(TArray<int32 >& board, int playerId);
	int32 minimax(TArray<int32 >& board, int playerId);

	int _nextTurnCellId;
};