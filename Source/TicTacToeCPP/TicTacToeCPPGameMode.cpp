// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeCPPGameMode.h"
#include "TicTacToeCPPPlayerController.h"
#include "TicTacToeCPPPawn.h"

ATicTacToeCPPGameMode::ATicTacToeCPPGameMode() {
	// no pawn by default
	DefaultPawnClass = ATicTacToeCPPPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ATicTacToeCPPPlayerController::StaticClass();
}
