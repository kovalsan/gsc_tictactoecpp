// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TicTacToeCPPGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ATicTacToeCPPGameMode : public AGameModeBase {
	GENERATED_BODY()

public:
	ATicTacToeCPPGameMode();
};



