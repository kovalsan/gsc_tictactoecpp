// Fill out your copyright notice in the Description page of Project Settings.

#include "BoardSolver.h"

// Sets default values for this component's properties
UBoardSolver::UBoardSolver() :
	_nextTurnCellId(-1)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UBoardSolver::BeginPlay() {
	Super::BeginPlay();
}


// Called every frame
void UBoardSolver::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UBoardSolver::PerformNextTurn(const TArray<int32>& board, int playerId) {
	int _nextTurnCellId = minimax(const_cast<TArray<int32>&>(board), playerId);
/*
	for (auto cell = board.CreateConstIterator(); cell; ++cell) {
		if (*cell == 0) {
			_nextTurnCellId = cell.GetIndex();
			break;
		}
	}
*/
	OnTurnAIComplete.Broadcast(_nextTurnCellId);
}

int UBoardSolver::maxSearch(TArray<int32 >& board, int playerId) {
	int opponentId = (playerId + 1) % 2;

	if (UBoardRules::GetGridState(board, opponentId) == EBoardState::BS_VICTORY)
		return 10;
	else if (UBoardRules::GetGridState(board, playerId) == EBoardState::BS_VICTORY)
		return -10;
	else if (UBoardRules::GetGridState(board, playerId) == EBoardState::BS_DRAW)
		return 0;

	int32 score = TNumericLimits<int32>::Min();

	for (auto& cell : board) {
		if (cell == static_cast<int32>(EPlayerType::PT_UNDEFINED)) {
			cell = static_cast<int32>(opponentId);
			int tmpScore = minSearch(board, playerId);

			if (tmpScore > score) {
				score = tmpScore;
			}

			cell = static_cast<int32>(EPlayerType::PT_UNDEFINED);
		}
	}

	return score;
}

int UBoardSolver::minSearch(TArray<int32 >& board, int playerId) {
	int opponentId = (playerId + 1) % 2;
	int32 score = TNumericLimits<int32>::Max();

	if (UBoardRules::GetGridState(board, opponentId) == EBoardState::BS_VICTORY)
		return 10;
	else if (UBoardRules::GetGridState(board, playerId) == EBoardState::BS_VICTORY)
		return -10;
	else if (UBoardRules::GetGridState(board, playerId) == EBoardState::BS_DRAW)
		return 0;

	for (auto& cell : board) {
		if (cell == static_cast<int32>(EPlayerType::PT_UNDEFINED)) {
			cell = static_cast<int32>(playerId);
			int tmpScore = maxSearch(board, playerId);

			if (tmpScore < score) {
				score = tmpScore;
			}

			cell = static_cast<int32>(EPlayerType::PT_UNDEFINED);
		}
	}

	return score;
}

int UBoardSolver::minimax(TArray<int32 >& board, int playerId) {
	int32 score = TNumericLimits<int32>::Max();
	int cellId = 0;
	int result = 0;

	for (auto& cell : board) {
		if (cell == static_cast<int32>(EPlayerType::PT_UNDEFINED)) {
			cell = static_cast<int32>(playerId);
			int tmpScore = maxSearch(board, playerId);

			if (tmpScore < score) {
				score = tmpScore;
				result = cellId;
			}

			cell = static_cast<int32>(EPlayerType::PT_UNDEFINED);
		}
		++cellId;
	}

	return result;
};


