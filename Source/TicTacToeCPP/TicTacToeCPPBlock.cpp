// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TicTacToeCPPBlock.h"
#include "TicTacToeCPPBlockGrid.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"

ATicTacToeCPPBlock::ATicTacToeCPPBlock() {
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> CrossMesh;
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> NoughtMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, CrossMesh(TEXT("/Game/Puzzle/Meshes/Brush_SM_Symb_X.Brush_SM_Symb_X"))
		    , NoughtMesh(TEXT("/Game/Puzzle/Meshes/Brush_SM_Symb_Zero.Brush_SM_Symb_Zero"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f,1.f,0.25f));
	BlockMesh->SetRelativeLocation(FVector(0.f,0.f,25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	BlockMesh->OnClicked.AddDynamic(this, &ATicTacToeCPPBlock::BlockClicked);
	BlockMesh->OnInputTouchBegin.AddDynamic(this, &ATicTacToeCPPBlock::OnFingerPressedBlock);

	PlaceHolder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh1"));
	PlaceHolder->SetRelativeLocation(FVector(0.f, 0.f, 70.f));
	PlaceHolder->SetupAttachment(DummyRoot);

	// Save pointers to the spawnable objects
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();

	NoughtMesh = ConstructorStatics.NoughtMesh.Get();
	CrossMesh = ConstructorStatics.CrossMesh.Get();
}

void ATicTacToeCPPBlock::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked) {
	if (OwningGrid->IsInteractable()) {
		HandleClicked();
	}
}


void ATicTacToeCPPBlock::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent) {
	if (OwningGrid->IsInteractable()) {
		HandleClicked();
	}
}

void ATicTacToeCPPBlock::HandleClicked() {
	// Check we are not already active and board intercation is allowed
	if (!bIsActive) {
		bIsActive = true;

		// Change material
		BlockMesh->SetMaterial(0, OrangeMaterial);

		// Tell the Grid
		if (OwningGrid != nullptr) {
			int playerId = static_cast<int32>(OwningGrid->CurrentPlayerId);
			UStaticMesh* symbol = playerId % 2 == 0 ? NoughtMesh : CrossMesh;
			PlaceHolder->SetStaticMesh(symbol);

			OwningGrid->UpdateGrid(CellId);
		}
	}
}

void ATicTacToeCPPBlock::Highlight(bool bOn) {
	// Do not highlight if the block has already been activated.
	if (bIsActive || !OwningGrid->IsInteractable()) {
		return;
	}

	if (bOn) {
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
	else {
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
}

void ATicTacToeCPPBlock::Reset() {
	BlockMesh->SetMaterial(0, BlueMaterial);
	bIsActive = false;
	PlaceHolder->SetStaticMesh(NULL);
}
